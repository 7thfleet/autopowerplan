/*
* Auto Power Plan
* Copyright (C) 2019 Jared White
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include <QStringList>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QCloseEvent>
#include "ui_mainwindow.h"

#include <iostream>

#include "mainwindow.h"
#include "PowerEventFilter.h"
#include "RegSettings.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    eventFilter(reinterpret_cast<HWND>(this->winId())){  //Set up our filter to catch power messages.



    //If the settings are bad, overwrite them with the defaults.
    if(!RegSettings::settingsValid()){
        qWarning() << "Invalid settings! Ignore if this is the first run.";
        RegSettings::setDefaults();
    }

    ui->setupUi(this);
    this->setWindowTitle(RegSettings::applicationName);

    updatePowerPlan();	//Ensure power plans are current

    //Create list of available power plans
    auto createPlanStrings = []()->QStringList{
            auto planVec = PowerManager::getAvailPlans();
            QStringList plans{};
            for(const auto& plan : planVec){
                plans.push_back(plan.first);
            }
            return plans;
    };


    //Set up and load UI options
        //For AC combo box
    ui->planAC_comboBox->addItems(createPlanStrings());
    ui->planAC_comboBox->setCurrentText(PowerManager::getPlanString(RegSettings::getPowerPlanAC()));

        //For bat combo box
    ui->planBattery_comboBox->addItems(createPlanStrings());
    ui->planBattery_comboBox->setCurrentText(PowerManager::getPlanString(RegSettings::getPowerPlanBattery()));

        //For auto start checkbox
    ui->autoStart_checkBox->setChecked(RegSettings::getIsAutoStart());



    createTrayIcon(parent); //Create the tray icon to allow remaximizing after closing window


    //Make clicking the "Exit" option in the tray menu close the application
    connect(trayMenu.get(), &QMenu::triggered, this,
            [=](QAction *action){
                QApplication::quit();
            }
    );



    //Make clicking the tray icon show the application
    connect(trayIcon.get(), &QSystemTrayIcon::activated, this,
            [=](QSystemTrayIcon::ActivationReason reason){
                if(reason == QSystemTrayIcon::Trigger){
                    this->show();
                    this->activateWindow();
                }
            }
    );

}


MainWindow::~MainWindow() {
    trayIcon->hide();   //Ensure tray icon goes away
    delete ui;
}


bool MainWindow::nativeEvent(const QByteArray& eventType, void *message, long* result){
    if(static_cast<MSG*>(message)){
        if(eventFilter.isPowerStatusChangeEvent(*static_cast<MSG*>(message))){
            qDebug() << "Received powerStatusChangeEvent message\n";
            updatePowerPlan();
        }
    }

    return QWidget::nativeEvent(eventType, message, result);	//Forward message
}


void MainWindow::updatePowerPlan(){
    GUID desiredPlan = GUID_NULL;

    if(PowerManager::isOnAC()){
        desiredPlan = RegSettings::getPowerPlanAC();
    }else{
        desiredPlan = RegSettings::getPowerPlanBattery();
    }

    if(!PowerManager::validGUID(desiredPlan)){
        RegSettings::setDefaults();
        qWarning() << "Bad desired plan, resetting to default.";
        return;
    }

    if(desiredPlan != PowerManager::getActivePowerPlan()){
        qDebug() << "Setting active power plan to " + PowerManager::getPlanString(desiredPlan);
        PowerManager::setPowerPlan(desiredPlan);
    }

    //Update "Current Power Plan" label
    ui->planCurrentStatus_label->setText(PowerManager::getPlanString(PowerManager::getActivePowerPlan()));
}


void MainWindow::createTrayIcon(QWidget* parent){
    trayIcon = std::make_unique<QSystemTrayIcon>(parent);
    trayIcon->setToolTip(RegSettings::applicationName);

    QIcon icon;
    icon.addPixmap(QPixmap(QStringLiteral(":/Resources/Images/PPS.png")), QIcon::Normal, QIcon::Off);   //Load app icon

    trayIcon->setIcon(icon);

    //Add a menu to the tray with "Exit" as an option
    trayMenu = std::make_unique<QMenu>("Menu", parent);
    trayMenu->addAction(QStringLiteral("Exit"));
    trayIcon->setContextMenu(trayMenu.get());

    trayIcon->show();

    //Make the same icon our titlebar icon
    this->setWindowIcon(icon);
}


void MainWindow::closeEvent(QCloseEvent* event){    //Close window, can be maximized with the tray icon.
    this->hide();
    event->ignore();    //Prevent the application from closing
}


void MainWindow::onPlanAC_ComboBoxChanged(const QString& newPlan){
    if(!PowerManager::validName(newPlan)){
        throw std::runtime_error("Invalid power plan value set.\n");    //Bad power plan was set somehow, abort
    }

    RegSettings::setPowerPlanAC(PowerManager::getPlanGUID(newPlan));
    updatePowerPlan();
}


void MainWindow::onPlanBattery_ComboBoxChanged(const QString& newPlan){
    if(!PowerManager::validName(newPlan)){
        throw std::runtime_error("Invalid power plan value set.\n");    //Bad power plan somehow set, abort
    }

    RegSettings::setPowerPlanBattery(PowerManager::getPlanGUID(newPlan));
    updatePowerPlan();
}


void MainWindow::onAutoStart_CheckboxChanged(bool autoStart){
    RegSettings::setIsAutoStart(autoStart);
}

