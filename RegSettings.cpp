/*
* Auto Power Plan
* Copyright (C) 2019 Jared White
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QSettings>
#include <QApplication>
#include <QDebug>

#include <Windows.h>
#include <winnt.h>

#include "RegSettings.h"
#include "PowerManager.h"

//Name of the registry settings.
const QString RegSettings::namePowerPlanAC      = QStringLiteral("PowerPlanAC");
const QString RegSettings::namePowerPlanBattery = QStringLiteral("PowerPlanBattery");

const QString RegSettings::autoStartPath        = QStringLiteral("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run");   //Proper path to add an application to so that it auto starts.

const QString RegSettings::autoStartArg         = QStringLiteral("-hide");          //Argument used to start the app in tray mode(doesn't show until tray icon is clicked)
const QString RegSettings::applicationName      = QStringLiteral("Auto Power Plan");


//Apply default settings
void RegSettings::setDefaults(){
    qDebug() << "Applying default settings:";
    qDebug() << "\tdefaultACPlan:\tHigh performance";
    qDebug() << "\tdefaultBatteryPlan:\tBattery saver";
    qDebug() << "\tAutoStart:\tYes";

    GUID defaultACPlan = GUID_MIN_POWER_SAVINGS;    //High Performance
    GUID defaultBatteryPlan = GUID_MAX_POWER_SAVINGS;   //Battery Saver

    setPowerPlanAC(defaultACPlan);
    setPowerPlanBattery(defaultBatteryPlan);

    setIsAutoStart(true);
}


//Auto Start Functions
bool RegSettings::getIsAutoStart(){
    QSettings settings(autoStartPath, QSettings::NativeFormat);
    return settings.value(applicationName).toBool();	//.toBool() returns false if DNE
}


void RegSettings::setIsAutoStart(bool autoStart){
    QSettings settings(autoStartPath, QSettings::NativeFormat);

    if(!autoStart){
        qDebug() << "Disabling autoStart\n";
        settings.remove(applicationName);
    }else{
        qDebug() << "Enabling autoStart\n";
        //Get path to executable to add to run key
        const QString execPath = "\"" +
                QCoreApplication::applicationFilePath().replace('/', '\\') +
                "\"" + " " + autoStartArg;
        settings.setValue(applicationName, execPath);
    }

    settings.sync();
}



bool RegSettings::settingsValid(){

    if(!PowerManager::validGUID(getPowerPlanAC())){
        return false;
    }

    if(!PowerManager::validGUID(getPowerPlanBattery())){
        return false;
    }

    return true;
}



//AC Functions
GUID RegSettings::getPowerPlanAC(){
    QSettings settings;
    QString acPlan = settings.value(namePowerPlanAC).toString();
    return PowerManager::getPlanGUID(acPlan);
}


void RegSettings::setPowerPlanAC(GUID plan){
    if(!PowerManager::validGUID(plan)){ //Ensure plan is valid
        qCritical() << "Invalid plan supplied to setPowerPlanAC";
        return;
    }

    QSettings settings;

    qDebug() << "Setting desired AC Plan to: " << PowerManager::getPlanString(plan);

    settings.setValue(namePowerPlanAC, PowerManager::getPlanString(plan));
    settings.sync();
}



//Battery Functions
GUID RegSettings::getPowerPlanBattery(){
    QSettings settings;
    QString batteryPlan = settings.value(namePowerPlanBattery).toString();
    return PowerManager::getPlanGUID(batteryPlan);
}


void RegSettings::setPowerPlanBattery(GUID plan){
    if(!PowerManager::validGUID(plan)){ //Ensure plan is valid
        qCritical() << "Invalid plan supplied to setPowerPlanBattery";
        return;
    }
    QSettings settings;

    qDebug() << "Setting desired Battery Plan to: " << PowerManager::getPlanString(plan);

    settings.setValue(namePowerPlanBattery, PowerManager::getPlanString(plan));
    settings.sync();
}
