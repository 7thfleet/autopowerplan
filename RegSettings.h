/*
* Auto Power Plan
* Copyright (C) 2019 Jared White
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef REGSETTINGS_H
#define REGSETTINGS_H

#include <QString>

#include <rpc.h>    //decl. of GUID

#include "PowerManager.h"

/*
 * This class controls the reading and writing of the application settings. It uses Qt's QSettings to handle most of this, which writes to the registry.
 * It also holds some application-wide information, such as the name of it.
 * It contains the implementation of the auto-start functionality.
 */

class RegSettings {
private:
    //Names of the registry values
    static const QString namePowerPlanAC;
    static const QString namePowerPlanBattery;

    //Path to registry key for auto starting
    static const QString autoStartPath;

public:
    static const QString autoStartArg;
    static const QString applicationName;

    //Write default values to registry
    static void setDefaults();

    //Check if application is set to auto start
    static bool getIsAutoStart();

    //Set or remove auto start
    static void setIsAutoStart(bool autoStart);


    //Returns true if registry values are valid. Will return false if values don't exist. @TODO check
    static bool settingsValid();


    //Returns the power plan stored in the registry. Returns GUID_NULL if invalid.
    static GUID	getPowerPlanAC();

    //Set the power plan to be used when on AC.
    static void setPowerPlanAC(GUID plan);


    //Returns the power plan stored in the registry. Returns GUID_NULL if invalid.
    static GUID getPowerPlanBattery();

    //Set the power plan to be used when on battery.
    static void setPowerPlanBattery(GUID plan);

};

#endif // REGSETTINGS_H
