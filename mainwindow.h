/*
* Auto Power Plan
* Copyright (C) 2019 Jared White
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include <QSystemTrayIcon>

#include <memory>

#include "PowerEventFilter.h"


/*
 * This class represents the actual application window. It controls the UI part of the application, as well as application messages.
 * It uses PowerEventFilter to listen for power status change methods. If found, uses PowerManager and RegSettings to get the desired plan and apply it.
 * The tray icon functionality is also implemented here.
 */

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

private:
    Ui::MainWindow *ui;
    PowerEventFilter eventFilter;               //Intercepts power status messages
    std::unique_ptr<QSystemTrayIcon> trayIcon;
    std::unique_ptr<QMenu> trayMenu;


private slots:
    void onPlanAC_ComboBoxChanged(const QString& newPlan);
    void onPlanBattery_ComboBoxChanged(const QString& newPlan);
    void onAutoStart_CheckboxChanged(bool autoStart);

protected:
    //Specialize to listen for power status messages, forwards events after checking them.
    bool nativeEvent(const QByteArray& eventType, void *message, long* result) override;

    //Change the active power plan to the desired one
    void updatePowerPlan();

    //Create a tray icon to allow exiting & maximizing after closing/hiding
    void createTrayIcon(QWidget* parent);

    //Minimize to tray instead of closing.
    void closeEvent(QCloseEvent* event) override;

};

#endif // MAINWINDOW_H
