Auto Power Plan
=======

### About
AutoPowerPlan is a small Windows application for getting the most out of your laptop or battery powered device. It allows you to chose a Power Plan for the battery, and a separate one to use when plugged in. For example, by default AutoPowerPlan will use "High Performance" when plugged in, and "Power Saver" when on battery. 
AutoPowerPlan minimizes to the tray, and can start with Windows in the background if needed. AutoPowerPlan now supports custom plans.

AutoPowerPlan is written in Modern C++ and Qt.

AutoPowerPlan is licensed under GPLv3. A copy of the license is available in the file LICENSE. 
The source code for this project can be found at gitlab.com/7thfleet/autopowerplan


### History
This is a complete rebuild of AutoPowerPlan. The old version(on github) was made before I started college.

This new version features the following improvements:

* Registers the application to receive power status messages, and uses the Windows message loop to listen for power source changes. The old version launched a background thread that checked every ~10 seconds.

* Better settings encapsulation through the RegSettings class.

* Modern C++: Use of smart pointers, auto, and lambdas resulted in a much more readable code base.

* Proper hiding of Windows functions: The C-Style Windows function calls are hidden within the PowerManager class. 

* General code improvements.



### TODO:
* Create standalone installer
* ~~Add license headers~~ Done.
* ~~Add support for non-default Power Plans~~ Done.
