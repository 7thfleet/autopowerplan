/*
* Auto Power Plan
* Copyright (C) 2019 Jared White
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include <QtGlobal>  //QAssert

#include <Windows.h>
#include <winnt.h>
#include <WinBase.h>
#include <powersetting.h>
#include <powrprof.h>

#include <memory>
#include <string>
#include <cerrno>
#include <system_error>

#include "PowerManager.h"

bool PowerManager::validGUID(GUID guid){
    auto plans = getAvailPlans();
    for(const auto& plan : plans){
        if(plan.second == guid){
            return true;        //Found plan, valid.
        }
    }

    return false;   //Not in our list of plans, invalid.
}

bool PowerManager::validName(const QString& name){
    auto plans = getAvailPlans();
    for(const auto& plan : plans){
        if(plan.first == name){
            return true;    //Found plans, valid
        }
    }

    return false;   //Not in our list of plans, invalid
}

GUID PowerManager::getPlanGUID(const QString& plan) {
    auto plans = getAvailPlans();
    for(const auto& planPair : plans){
        if(planPair.first == plan){
            return planPair.second;
        }
    }

    return GUID_NULL;
}

QString PowerManager::getPlanString(GUID plan){
    Q_ASSERT_X(plan != GUID_NULL, "getPlanString", "plan was an invalid GUID(GUID_NULL)");

    DWORD bufferSize = 0;
    std::unique_ptr<UCHAR[]> buffer = std::make_unique<UCHAR[]>(bufferSize);

    DWORD result = PowerReadFriendlyName(   //Calling this with a buffer that is too small puts the needed size into bufferSize
                nullptr,
                &plan,
                nullptr,
                nullptr,
                buffer.get(),   //Interesting side note: using NULL instead of an actual valid pointer to an empty buffer returns the wrong error code (ERROR_SUCCESS instead of ERROR_MORE_DATA)
                &bufferSize);
    if(result != ERROR_MORE_DATA){  //Call somehow succeeded with size 0, something must have broken. Abort.
        throw std::system_error(ENOTRECOVERABLE, std::system_category(), "PowerReadFriendlyName(1st call) did not return ERROR_MORE_DATA as expected.");
    }


    //bufferSize now holds required length of the buffer

    buffer = std::make_unique<UCHAR[]>(bufferSize);

    result = PowerReadFriendlyName(
                nullptr,
                &plan,
                nullptr,
                nullptr,
                buffer.get(),
                &bufferSize);

    if(result != ERROR_SUCCESS){  //Unrecoverable.
        throw std::system_error(ENOTRECOVERABLE, std::system_category(), "PowerReadFriendlyName(2nd call) did not return ERROR_SUCCESS as expected.");
    }

    std::wstring wname{};   //buffer is unicode wide string

    for(std::size_t i=0; i<bufferSize; i++){
        if(buffer[i]){          //The string has null chars inserted between letters, don't add them.
            wname+=buffer.get()[i];
        }
    }

    return QString::fromStdWString(wname);
}

void PowerManager::setPowerPlan(GUID plan){
    Q_ASSERT_X(plan != GUID_NULL, "setPowerPlan", "plan == GUID_NULL");

    if(PowerSetActiveScheme(nullptr, &plan)){
        qWarning() << "Failed to set power plan.";    //Errored. Nothing we can do, but probably not deadly.
    }
}

GUID PowerManager::getActivePowerPlan(){
    GUID* activeSchemeGUID = nullptr;

    if(PowerGetActiveScheme(nullptr, &activeSchemeGUID)){   //Allocates memory, free later.
        qWarning() << "Failed to get active power plan.";
        if(activeSchemeGUID != nullptr){
            LocalFree(activeSchemeGUID);
        }

        return GUID_NULL;
    }

    GUID plan = *activeSchemeGUID;
    LocalFree(activeSchemeGUID);

    return plan;
}

bool PowerManager::isOnAC(){
    auto powerStatus = std::make_unique<SYSTEM_POWER_STATUS>();

    if(!GetSystemPowerStatus(powerStatus.get())){
        qWarning() << "Failed to get system power status.";   //Not our fault and nothing we can do
        return false;
    }

    return (powerStatus->ACLineStatus);
}

std::vector<std::pair<QString, GUID>> PowerManager::getAvailPlans(){

    std::vector<std::pair<QString, GUID>> plans = {
        {getPlanString(GUID_MIN_POWER_SAVINGS),     GUID_MIN_POWER_SAVINGS},
        {getPlanString(GUID_TYPICAL_POWER_SAVINGS), GUID_TYPICAL_POWER_SAVINGS},
        {getPlanString(GUID_MAX_POWER_SAVINGS),     GUID_MAX_POWER_SAVINGS}};
    //These plans exist on every computer, but PowerEnumerate doesn't always list them for some reason. When this happens, they also won't show up in the Control Panel. They can still be accessed with their GUID(And are often the plans we want the most, such as high perf or power saver), so we add them manually, and just avoid re-adding them.

    auto getScheme=[](ULONG index, GUID* buffer){   //Put GUID at index into buffer. Start with index = 0 and increase until it does not return ERROR_SUCCESS (0)
            DWORD bufferSize = sizeof(GUID);
            return PowerEnumerate(
                        nullptr,
                        nullptr,
                        nullptr,
                        ACCESS_SCHEME,
                        index,
                        reinterpret_cast<UCHAR*>(buffer),
                        &bufferSize);
    };

    auto containsGUID = [](const std::vector<std::pair<QString, GUID>>& vec, GUID guid){    //Check if our vector contains a specific GUID
        for(const auto& p : vec){
            if(p.second == guid){
                return true;    //Found
            }
        }
        return false;
    };

    GUID currPlan = GUID_NULL;
    for(ULONG i = 0; !getScheme(i, &currPlan); i++){
        if(!containsGUID(plans, currPlan)){     //We don't have the plan yet
            plans.push_back({getPlanString(currPlan), currPlan});
        }
    }

    Q_ASSERT_X(!containsGUID(plans, GUID_NULL), "getAvailPlans", "Plan list contains GUID_NULL.");    //We should not have an invalid plan.

    return plans;
}


