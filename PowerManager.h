/*
* Auto Power Plan
* Copyright (C) 2019 Jared White
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POWERMANAGER_H
#define POWERMANAGER_H

#include <QString>

#include <Windows.h>
#include <WinUser.h>

#include <vector>
#include <utility>

/*
 * This class has two primary tasks:
 *      1. Get and set various system attributes (AC status, current power plan)
 *      2. Provide a robust means of converting between the QString and GUID representation of the power plans (Or schemes, as Microsoft calls them).
 * The entire class is static, with no internal state.
 */

class PowerManager{

public:

    //Checks if supplied GUID is a valid power plan GUID.
    static bool validGUID(GUID guid);

    //Check if supplied QString is a valid power plan name
    static bool validName(const QString& name);

    //Returns the GUID of the supplied plan. Returns GUID_NULL if plan doesn't exist
    static GUID getPlanGUID(const QString& plan);

    //Get the string/friendly name of a plan from a GUID
    static QString getPlanString(GUID plan);

    //Sets the active plan.
    static void setPowerPlan(GUID plan);

    //Returns the active plan GUID.
    static GUID getActivePowerPlan();

    //Returns true if on AC power(plugged in)
    static bool isOnAC();

    //Get a list of the names and GUIDs of all available plans
    static std::vector<std::pair<QString, GUID>> getAvailPlans();

};

#endif // POWERMANAGER_H
