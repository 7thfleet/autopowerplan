/*
* Auto Power Plan
* Copyright (C) 2019 Jared White
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POWEREVENTFILTER_H
#define POWEREVENTFILTER_H

#include "Windows.h"
#include "WinUser.h"

/*
 * This class is used to intercept power status messages.
 * If a message is not a power status message, it is forwarded to the main Qt application.
*/

class PowerEventFilter {
private:
    HPOWERNOTIFY unregHandle; //Handle used to unregister when destroyed

public:
    PowerEventFilter(const HWND& handle);
    ~PowerEventFilter();
    bool isPowerStatusChangeEvent(const MSG& message) const;
};

#endif // POWEREVENTFILTER_H
