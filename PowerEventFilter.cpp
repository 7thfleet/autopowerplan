/*
* Auto Power Plan
* Copyright (C) 2019 Jared White
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDebug>

#include <Windows.h>
#include <WinBase.h>
#include <WinUser.h>
#include <winnt.h>
#include <objbase.h>
#include <combaseapi.h>

#include <cerrno>
#include <system_error>

#include "PowerEventFilter.h"

PowerEventFilter::PowerEventFilter(const HWND& handle) {
    if(!handle){
        qCritical() << "Unable to acquire application handle! Aborting.";
        throw std::system_error(EINVAL, std::system_category(), "Invalid window handle supplied to event filter.\n");
    }

    unregHandle = RegisterPowerSettingNotification(handle, &GUID_ACDC_POWER_SOURCE, DEVICE_NOTIFY_WINDOW_HANDLE);

    if(!unregHandle){
        qCritical() << "Unable to register for power change notifications! Aborting.";
        throw std::system_error(ENOTRECOVERABLE, std::system_category(), "Failed to register for power change notifications.\n");
    }
}

PowerEventFilter::~PowerEventFilter() {
    if(!UnregisterPowerSettingNotification(unregHandle)){
        try {   //IO can throw
            qWarning() << "Failed to unregister for power change notifications.";
        } catch (...) {
                //Nothing to do.
        }
    }

    unregHandle = nullptr;
}

bool PowerEventFilter::isPowerStatusChangeEvent(const MSG& message) const {
    return (WM_POWERBROADCAST == message.message && PBT_APMPOWERSTATUSCHANGE == message.wParam);    //Check if power source has changed
}
